/* == SETUP == */
/* General */
const loopTasks = (tasks, cbFn) => {
    return Object.getOwnPropertyNames(tasks)
        .reduce((container, taskName) => {
            const task = tasks[taskName];
            return cbFn(task);
        }, null);
};
const Config = require('./gulpfile.config');
const gulp = require('gulp');
const $ = {};

/* Gulp Plugins */
// Run Cmd (node built-in)
$.runCmd = require('child_process').exec;

// Html
$.fileIncl = require('gulp-file-include');
$.btfy = require('gulp-jsbeautifier');
$.pug = require('gulp-pug');

// CSS
$.sass = require('gulp-sass');
$.autoprefixer = require('gulp-autoprefixer');

// Typescript/JavaScript
$.browserify = require('browserify');
$.vinylBuffer = require('vinyl-buffer');
$.vinylStream = require('vinyl-source-stream');
$.tsify = require('tsify');
$.tslint = require('gulp-tslint');
$.uglify = require('gulp-uglify');

// Source Map (Css & TS)
$.sourcemaps = require('gulp-sourcemaps');

// Image Sprite generation
$.spritesmith = require('gulp.spritesmith');

// Unit Test Runner (Karma)
$.karma = require('karma').Server;

// Util: Server/Testing
$.browserSync = require('browser-sync').create();

// Util: Version
$.bump = require('gulp-bump');

// Util: General
$.if = require('gulp-if'),     // e.g. $.if(condition, fnToRun;
$.yargs = require('yargs').argv;
$.rename = require('gulp-rename');
$.clean = require('gulp-clean');
$.zip = require('gulp-zip');

// Util: Log
$.taskListing = require('gulp-task-listing');
$.runTasks = require('run-sequence');
$.plumber = require('gulp-plumber');
$.print = require('gulp-print');

/* == TASKS == */
/* Build */
// Run a cmd
/* Example:
gulp.task('build-spa', (done) => {
    $.runCmd('ng build --prod', (err, stdout, stderr) => {
        console.log(stdout);
        console.log(stderr);
        done(err);
      });
});
*/

// Build All Pipeline
gulp.task('build', () => {
    $.runTasks(
        'clean',
        'build-html',
        'build-ts',
        'build-css',
        'build-copy'
    );
});

// Run Local Server
gulp.task('serve', () => {
    // clean has to be separated from build
    // $.runTasks('clean', 'build-ts', ...,  'watch') or $.runTasks('build', 'watch') does not work unfortunately due to plugin bug
    $.runTasks(
        'clean',
        'build-html',
        'build-ts',
        'build-css',
        'build-copy',
        'watch'
    );
});

// Html
gulp.task('build-html', () => {
    const { tasks, defOption } = Config.html.pug;

    return loopTasks(tasks, (task) => {
        return gulp.src(task.inputFiles)
            .pipe( $.plumber() )
            .pipe( $.pug() )
            .pipe( gulp.dest(task.outputPath) );
    });
});

// Sass
gulp.task('build-css', [], () => {
    const { tasks } = Config.css.sass;

    return loopTasks(tasks, (task) => {
        return gulp.src(task.inputFiles)
            .pipe( $.plumber() )
            .pipe( $.if(!$.yargs.prod, $.sourcemaps.init()) )
            .pipe( $.sass({outputStyle: $.yargs.prod ? 'compressed' : 'nested' }) )
            .pipe( $.autoprefixer(Config.css.autoprefixer.defOption) )
            .pipe( $.if(!$.yargs.prod, $.sourcemaps.write()) )  // Use Inline Sourcemap at LocalBuild
            .pipe( $.rename({extname: '.min.css'}) )
            .pipe( gulp.dest(task.outputPath) );
    });
});

// JS/TS
gulp.task('build-ts', ['build-ts:lint'], () => {
    const { tasks, defOption } = Config.ts.compile;

    return loopTasks(tasks, (task) => {
        return $.browserify({
            basedir: defOption.basePath,
            entries: task.inputFiles,
            debug: !$.yargs.prod,           // true: generate sourcemap
            cache: {},
            packageCache: {}
        })
        .plugin( $.tsify )                  // https://www.npmjs.com/package/tsify
        .transform('babelify', {
            presets: defOption.targetEsVersion,
            extensions: [ '.ts' ]
        })
        .bundle()
        .pipe( $.plumber() )
        .pipe( $.vinylStream(task.outputFile) )
        .pipe( $.vinylBuffer() )
        .pipe( $.if(!$.yargs.prod, $.sourcemaps.init({loadMaps: true})) )   // loadMaps: true (use existing sourcemp from TS)
        .pipe( $.if(!$.yargs.prod, $.sourcemaps.write()) )                  // inline sourcemap
        .pipe( $.if($.yargs.prod, $.uglify()) )
        .pipe( $.rename({ extname: '.min.js'}) )
        .pipe( gulp.dest(task.outputPath) );
    });
});

// Linting (this uses the avail. "tslint.json" file)
gulp.task('build-ts:lint', () => {
    const { tasks, defLintOption, defReportOption } = Config.ts.lint;

    return loopTasks(tasks, (task) => {
        return gulp.src(task.inputFiles)
            .pipe( $.plumber() )
            .pipe( $.tslint(defLintOption) )
            .pipe( $.tslint.report(defReportOption) );
    });
});

// Build Image Sprite
gulp.task('build-img', () => {
    const { defOption, tasks } = Config.img.spritesmith;

    return loopTasks(tasks, (task) => {
        const spriteData = gulp.src(task.inputImgFiles)
            .pipe( $.plumber() )
            .pipe( $.spritesmith(defOption) );

        spriteData.css.pipe( gulp.dest(task.outputScssPath) );
        spriteData.img.pipe( gulp.dest(task.outputImgPath) );

        return spriteData;
    });
});

// Files that need to be copied across
gulp.task('build-copy', () => {
    const { tasks } = Config.util.copy;

    return loopTasks(tasks, (task) => {
        return gulp.src( task.inputFiles )
            .src( $.plumber() )
            .pipe( gulp.dest(task.outputPath) );
    });
});

// Unit Testing (https://github.com/karma-runner/gulp-karma)
gulp.task('test', (done) => {
    const watch = $.yargs.watch;
    const { defOption } = Config.karma;

    if (watch) defOption.singleRun = false;

    $.karma.start(defOption,  (karmaResult) => {
        done();
    });
});


/* Utility */
// Task Listing
gulp.task('tasks', $.taskListing);

// Clean up
gulp.task('clean', () => {
    const { tasks } = Config.util.clean;

    return loopTasks(tasks, (task) => {
        return gulp.src( task.inputFiles, {read: false} )
            .pipe( $.plumber() )
            .pipe( $.print() )
            .pipe( $.clean() );
    });
});

// Monitor & Refresh Browser
gulp.task('watch', () => {
    const { defOption, watchFiles } = Config.util.browserSync;

    $.browserSync.init(defOption);
    gulp.watch( watchFiles.src.html, ['build-html'] );
    gulp.watch( watchFiles.src.scss, ['build-css'] );
    gulp.watch( watchFiles.src.ts, ['build-ts'] );
    gulp.watch( watchFiles.src.img, ['build-img'] );
    gulp.watch( watchFiles.dist).on('change', $.browserSync.reload);
});

// Verion Bumping
gulp.task('bump', () => {
    /**
     * The following are for Input in command line:
     * --type=pre                   // bump the prerelease version      *.*.*-x
     * --type=patch OR no flag      // bump the patch version           *.*.x
     * --type=minor                 // bump the minor version           *.x.*, e.g. 0.1.0 --> 0.2.0
     * --type=major                 // bump the major version           x.*.*
     * --version=1.2.3              // bump to a specific version & ignore other flags
     */
    let msg = 'Bumping version';
    const { version, type } = $.yargs;
    const options = {};

    // If user provide a specific version in the cmd
    if (version) {
        options.version = version;
        msg += ' to ' + version;

    // If specific version is not provided
    } else {
        options.type = type;
        msg += ' for a ' + type;
    }

    // Bumping up the version in json files
    return gulp
        .src([ './package.json' ])
        .pipe( $.bump(options) )
        .pipe( gulp.dest('./') );
});
