/*
 *  Required Format for Addition/Modification
 *  ==========================================
 *  <groupName>: {
 *      <pluginName1>: {
 *          defOption: {        // <an object or string for the plugin setting>,
 *              <prop1>: val1
 *              <propN>: valN
 *          }
 *          tasks: {
 *              <taskName>: {
 *                  inputFiles:
 *                  outputPath:
 *                  outputFile:
 *                  overrideSetting:
 *              }
 *          }
 *      }
 *  }
 */

module.exports = {
    // HTML
    html: {
        pug: {
            defOption: {

            },
            tasks: {
                index: {
                    inputFiles: [ './src/*.pug' ],
                    outputPath: './dist'
                }
            }
        }
    },

    // CSS
    css: {
        autoprefixer: {
            defOption: {
                browsers: ['last 10 version', 'IE 9'],
                ifCascade: false
            }
        },
        sass: {
            tasks: {
                global: {
                    inputFiles: './src/scss/main.scss',              // NOTE: wildcard "*" is not allowed
                    outputPath: './dist/css/'
                },
            }
        }
    },

    // TYPESCRIPT
    ts: {
        compile: {
            defOption: {
                basePath: './src',
                targetEsVersion: [ 'es2015' ]
            },
            tasks: {
                main: {
                    // NOTE: as this is supposed to be entry file, wildcard `**` is not allowed
                    inputFiles: [
                        // required if you import it - does not make diff. if you do `import * as $ from 'jquery';`
                        '../node_modules/jquery/dist/jquery.min.js',
                        'ts/main.ts'
                    ],
                    outputFile: 'main.js',
                    outputPath: './dist/js'
                }
            }
        },
        lint: {
            defLintOption: {
                // formatter: 'verbose'      will cause warning to emit error
                // formatter: 'prose'
            },
            defReportOption: {
                allowWarnings: false
            },
            tasks: {
                main: {
                    inputFiles: [ './src/ts/*.ts' ]
                }
            }
        }
    },

    // IMG
    img: {
        spritesmith: {
            defOption: {
                imgName: 'icons.png',
                cssName: '_icons.scss',
                algorithm: 'left-right'     // top-down | left-right | diagonal | alt-diagonal | binary-tree
            },
            tasks: {
                main: {
                    inputImgFiles: [ 'src/img/sprite/*.png' ],
                    outputImgPath: 'dist/img/',
                    outputScssPath: 'src/scss/_common/'
                }
            }
        }
    },

    // UTILITY
    util: {
        // Clean & Copy
        clean: {
            tasks: {
                dist: {
                    inputFiles: [
                        // 'dist/*'      // all folders under "dist"
                        './dist/'        // "dist" folder
                    ]
                }
            }
        },
        copy: {
            tasks: {
                // html: {
                //     inputFiles: [ 'src/index.html', ],
                //     outputPath: 'dist/'
                // }
            }
        },

        // Server & Watch Files
        browserSync: {
            defOption: {
               server: { baseDir: './dist/' },
            //    startPath: '/#/',
               reloadDelay: 1000
            },
            watchFiles: {
                src: {
                    html: [
                        './src/*.pug',
                    ],
                    scss: [
                        './src/scss/*.scss',
                        './src/scss/**/*.scss'
                    ],
                    ts: [
                        './src/ts/*.ts',
                        '!./src/ts/*.spec.ts',
                    ],
                    img: [
                        './src/img/*.png',
                        './src/img/sprite/*.png',
                    ]
                },
                dist: [
                    './dist/index.html',
                    './dist/css/*.css',
                    './dist/js/*.js',
                    './dist/img/*.*'
                ]
            }
        }
    },

    // UNIT TESTING
    karma: {
        // defOption: {
        //     configFile: __dirname + "/karma.conf.gulp.js",
        //     singleRun: true
        // },
        defOption: {
            /* 1. FILES TO BE LOADED/EXCLUDED IN THE BROWSER (SHOULD BE IN ORDER) */
            basePath: './src',                 // relative to Karma config file
            files: [
                // 1. Library, e.g. jquery, angular
                '../node_modules/jquery/dist/jquery.min.js',

                // 2. App related Ts/Js files, e.g. main.ts
                'ts/main.ts',
                // '!ts/*.spec.ts'

                // 3. App related spec/test files, e.g. main.spec.ts
                'ts/*.spec.ts',
                // 'ui/**/**/*.spec.ts'
            ],
            exclude: [],
            autoWatch: true,    // monitor file changes

            /* 2. LOG */
            colors: true,
            // logLevel: config.LOG_INFO,       // def: config.LOG_INFO

            /* 3. PLUGINS & PREPROCESSOR */
            // List of Plugins to be loaded for testing, generating reports, launching browser, preprocessing files etc
            plugins: [
                'karma-typescript',
                'karma-chrome-launcher',
                'karma-jasmine',
                'karma-spec-reporter',
                'karma-coverage'
            ],

            // List of Files to be preprocessed by Preprocessors Plugin
            // - not configured here, refer to "config.preprocessors" at the bottom of the code
            // - e.g.
            //     // show coverage stats in PhantomJS Console/CMD
            //     'build/js/*.js': ['coverage'],
            //
            //     // process the html files with 'karma-ng-html2js-preprocessor' plugin
            //     'build/view/*.html': ['ng-html2js']
            preprocessors: {
                'ts/*.ts': [
                    'karma-typescript',     // corresponds to 'karma-typescript'
                    'coverage'
                ],
            },

            /* 4. PLUGIN CONFIG (INCL. PREPROCESSOR) */
            // Plugin Config - Browser
            browsers: ['Chrome'],           // Browser to be launched to run the test (requires "karma-<browsername>-launcher" plugin)
            port: 8090,
            singleRun: true,

            // Plugin Config - Testing Framework
            frameworks: [
                'karma-typescript',         // corresponds to 'karma-typescript'
                'jasmine',                  // installed below as npm package 'karma-jasmine' therefore no required to include js
            ],

            // Plugin Config - Report Coverage
            reporters: [
                'spec',                 // corresponds to 'karma-spect-reporter'
                'coverage'              // corresponds to 'karma-coverage'
            ],
            specReporter: {
                maxLogLines: 5,         // limit number of lines logged per test
                suppressErrorSummary: true,  // do not print error summary
                suppressFailed: false,       // do not print information about failed tests
                suppressPassed: false,       // do not print information about passed tests
                suppressSkipped: true,       // do not print information about skipped tests
                showSpecTiming: false        // print the time elapsed for each spec
            },
            coverageReporter: {
                type: 'text-summary'
            }
        }
    }

}; // end module.exports
