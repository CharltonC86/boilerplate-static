/*
Importing jQuery
------------------------------
The following is outdated:
/// <reference path ="../../node_modules/@types/jquery/index.d.ts" />

Use ES6 import instead:
import * as $ from 'jquery';
*/

import * as $ from 'jquery';

// $('body').css('background', 'red');
