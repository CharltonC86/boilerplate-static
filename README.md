# About

### Purpose
This boilerplate serves the purpose of building static site with: 
- SCSS (CSS)
- JADE (HTML)
- TypeScript 2.5.3
- Node 10.8.0 | Npm 6.14.4

### Setup (Mac based) 
1. Install Node 6.9.0 or higher
2. Install Typescript in Terminal:  
    ```
    npm install -g typescript
    ```
3. Under Project Root, Install the dependencies in Terminal:  
    ```
    npm install
    ```

### VisualStudio Code Editor Setup
* Go to Menu: Preference > Settings, in your user settings, make sure the settings has the following set:  
```javascript
{
    ...
    "typescript.tsdk": "node_modules/typescript/lib",
    "files.trimTrailingWhitespace": true,
    "[markdown]": {
        "files.trimTrailingWhitespace": false
    }        
}
```

### CLI Command
##### Static Project (refer to `gulpfile.js` file for the rest)
* Build (dev/prod):
    ```
    gulp build
    ```
    or
    ```
    gulp build --prod
    ```    
* Build Scss/Css:
    ```
    gulp build-css
    ```
* Build Typescript (including linting):
    ```
    gulp build-ts
    ```
* Lint Typescript:
    ```
    gulp build-ts:lint
    ```
* Unit Test Typescript:
    ```
    gulp test
    ```    
* Development server (`http://localhost:3000/`):  
    ```
    gulp serve
    ```
* Generate Docs:
    ```
    npm run doc
    ```

##### Common Command
* Generate Directory Tree (required to be copied from terminal upon generation & added to README):  
    ```
    npm run dir-tree
    ```   

### Folder Structure - Static & Single-Page-App Project (angular)
    node_modules/               // dev dependencies
    gulpfile.js                 // build logic
    gulpfile.config.js          // build config
    .esdoc.json                 // doc plugin config
    tsconfig.json               // ts config
    tslint.json                 // ts linting  config

    documentation/              // Documentation generated
    dist/                       // Output folder
    src/                        // Source code
        img/                    // images
        scss/                   // main scss files            
        ts/                     // main javascript/typescript files
        index.pug               // index.html template